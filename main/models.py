from django.db import models

# Create your models here.

# Main model for an expense item.
class Expense(models.Model):
	date = models.DateField()
	category = models.CharField(max_length = 256)
	name_emp = models.CharField("employee name", max_length = 256)
	address = models.CharField("employee address", max_length = 1024)
	description = models.CharField("expense description", max_length = 256)
	amount_pretax = models.DecimalField("pre-tax amount", max_digits = 20, decimal_places = 2) # I'd hate to be the accountant processing a 20-digit expense claim, but..
	name_tax = models.CharField("tax name", max_length = 256)
	amount_tax = models.DecimalField("tax amount", max_digits = 10, decimal_places = 2)

	def __str__(self):
		return "{d}: Expense of {a} made by {e}.".format(
				d = self.date,
				a = self.amount_pretax + self.amount_tax,
				e = self.name_emp
			)

