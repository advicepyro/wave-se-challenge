from django.conf.urls import url

from . import views

urlpatterns = [
	url(r'^$', views.main_page, name='index'),
	url(r'^upload/$', views.import_csv_data, name='upload'),
	url(r'^purge/$', views.purge, name='purge'),
]