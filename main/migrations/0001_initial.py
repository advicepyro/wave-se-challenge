# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Expense',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('date', models.DateField()),
                ('category', models.CharField(max_length=256)),
                ('name_emp', models.CharField(max_length=256, verbose_name='employee name')),
                ('address', models.CharField(max_length=1024, verbose_name='employee address')),
                ('description', models.CharField(max_length=256, verbose_name='expense description')),
                ('amount_pretax', models.DecimalField(decimal_places=2, max_digits=20, verbose_name='pre-tax amount')),
                ('name_tax', models.CharField(max_length=256, verbose_name='tax name')),
                ('amount_tax', models.DecimalField(decimal_places=2, max_digits=10, verbose_name='tax amount')),
            ],
        ),
    ]
