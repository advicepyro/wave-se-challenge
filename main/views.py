from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.db import connection
import csv
from .models import Expense
from datetime import datetime

# Create your views here.

# Nukes the Expenses table
def purge(request):

	if request.POST['delete'] == "true":
		Expense.objects.all().delete()
	return HttpResponseRedirect("/")


# Called by the upload form. Takes in an uploaded CSV file
# and stores each record.
def import_csv_data(request):
	# Open uploaded file as string. Then split apart
	# the newlines into strings
	uploaded_file = request.FILES['csvfile'].read().decode().splitlines()

	# Open a new CSV reader instance and bypass the header line.
	# Here we avoid inserting duplicate entries in the database, allowing
	# the user to reimport a CSV if it contains old data
	reader = csv.reader(uploaded_file)
	next(reader)
	for row in reader:
		# The DateTime field in our model expects dates as YYYY-MM-DD.
		# The imported CSV in this case has them as MM/DD/YYYY.
		# Let's convert. Answer from http://stackoverflow.com/a/14524356
		datestamp = datetime.strptime(row[0], "%m/%d/%Y").strftime("%Y-%m-%d")

		# Note for below: I'm assuming we're using US-style money amounts only ($1,999.00)
		Expense.objects.get_or_create(
			date = datestamp,
			category = str(row[1]),
			name_emp = str(row[2]),
			address = str(row[3]),
			description = str(row[4]),
			amount_pretax = float(row[5].replace(",", "")),
			name_tax = str(row[6]),
			amount_tax = float(row[7].replace(",", "")),
			)
	return HttpResponseRedirect("/")

def main_page(request):

	# Grab the table of expense data
	summary_data = []

	with connection.cursor() as cursor:
		cursor.execute('select sum(amount_pretax) as total_pretax, sum(amount_tax) as total_tax, strftime("%m", date) as expense_month, strftime("%Y", date) as expense_year from `main_expense` group by expense_year, expense_month order by expense_year desc, expense_month desc;')
		row = cursor.fetchone()
		while row is not None:
			total_pretax, total_tax, expense_month, expense_year = row

			# Now let's do a little bit of extra info - figure out who claimed the most expenses this month
			top_spender = ""
			with connection.cursor() as inner_cursor:
				inner_cursor.execute('select sum(amount_pretax + amount_tax) as total_expenses, name_emp from main_expense where date like "{d}" group by name_emp order by total_expenses desc limit 1;'.format(d = str(expense_year) + "-" + str(expense_month) + "%"))
				inner_row = inner_cursor.fetchone()
				if inner_row:
					_, top_spender = inner_row

			# We'll prettify some of the return data below.
			# Also, change the month name to be in wording
			expense_month_name = datetime.strptime(expense_month, "%m").strftime("%B")
			summary_data.append({
				"total": "$" + format(total_pretax + total_tax, ".2f"),
				"total_pretax": "$" + format(total_pretax, ".2f"),
				"total_tax": "$" + format(total_tax, ".2f"),
				"expense_month": expense_month_name,
				"expense_month_number": expense_month,
				"expense_year": expense_year,
				"top_spender": top_spender,
				})
			row = cursor.fetchone()

	return render(request, 'main/index.html', {
		"expense_list": Expense.objects.all(),
		"summary_data": summary_data,
		})