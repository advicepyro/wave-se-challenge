# README #

A sample web application built using Python Django. A (cleaned up) fork of the Wave SE challenge: https://github.com/wvchallenges/se-challenge

# Usage #

Run **python3 manage.py runserver** from the terminal. No setup required.